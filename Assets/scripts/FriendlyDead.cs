﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class FriendlyDead : MonoBehaviour
{
    public GameObject particlePrefab;
    public GameObject player;
    public GameObject endScreen;
    public UnityEvent gameEnd;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "projectile")
        {
            
            Vector3 pos = collision.gameObject.transform.position;
            Quaternion rot = collision.gameObject.transform.rotation;
            Instantiate(particlePrefab, new Vector3(pos.x, pos.y, pos.z - 5), Quaternion.Euler(rot.x, 90, rot.z));

            Instantiate(endScreen, transform.position, Quaternion.Euler(0, 0, 0));
            player.GetComponent<BoxCollider2D>().isTrigger = false;
            player.GetComponent<CircleCollider2D>().isTrigger = false;
            player.GetComponentInParent<Animator>().enabled = false;
            player.GetComponent<Rigidbody2D>().gravityScale = 1;
            gameEnd.Invoke();
        }

    }

}
