﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class BossReaction : MonoBehaviour
{
    
    public GameObject particlePrefab;
    public GameObject endScreen;
    public UnityEvent PlayerFail;
    public UnityEvent PlayerSuccess;
    public UnityEvent winning;
    public UnityEvent shoot;
    public bool playerKill;
    public GameObject weapon;
    public int bossHealth = 5;


    private void OnTriggerEnter2D(Collider2D collision)
    {

        if (collision.gameObject.tag == "projectile")
        {
            Vector3 pos = collision.gameObject.transform.position;
            Quaternion rot = collision.gameObject.transform.rotation;
            Instantiate(particlePrefab, new Vector3(pos.x, pos.y, pos.z - 5), Quaternion.Euler(rot.x, 90, rot.z));
            playerKill = true;
            PlayerSuccess.Invoke();
            bossHealth--;
            if (bossHealth <= 0)
            {

                Destroy(gameObject);
                Instantiate(particlePrefab, transform.position, Quaternion.Euler(0,0,0));
                Instantiate(endScreen, transform.position, Quaternion.Euler(0, 0, 0));
                winning.Invoke();
                Debug.Log("finish");
            }
            
        }
        else
        {
            if (collision.gameObject.tag == "bomb")
            {
                Debug.Log("boom");
                playerKill = true;
                weapon.GetComponent<LookAt>().enabled = false;
                weapon.GetComponent<Rigidbody2D>().gravityScale = 1;
                weapon.GetComponent<PolygonCollider2D>().enabled = true;
            }
        }

    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        
        if (collision.gameObject.name == "marker")
        {
            if ((gameObject.GetComponent<BoxCollider2D>().IsTouching(collision) == false) && (gameObject.GetComponent<CircleCollider2D>().IsTouching(collision) == false))
            {
                StartCoroutine(FailEvent(collision.gameObject));
                
            }
        }
        else
        {
            if (collision.gameObject.tag == "projectile")
            {
                Vector3 pos = collision.gameObject.transform.position;
                Quaternion rot = collision.gameObject.transform.rotation;
                Instantiate(particlePrefab, new Vector3(pos.x, pos.y, pos.z - 5), Quaternion.Euler(rot.x, -90, rot.z));
            }
        }

    }

    public void PreShooting()
    {
        StartCoroutine(PreShoot());
    }


    public void NextState()
    {
        playerKill = false;
    }

    IEnumerator FailEvent(GameObject marker)
    {
        yield return new WaitForSeconds(0.5f);

        if ((playerKill == false) && (bossHealth > 0))
        {

            PlayerFail.Invoke();
        }

    }

    IEnumerator PreShoot()
    {
        while (bossHealth > 0)
        {
            yield return new WaitForSeconds(1);
            shoot.Invoke();
            if (playerKill == true) { break; }
        }
        
    }

    
}
