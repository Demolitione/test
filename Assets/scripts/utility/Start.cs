﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Start : MonoBehaviour
{
    public Scene mainScene;

    public void StartScene()
    {
        SceneManager.LoadScene("gameField");
    }
}
