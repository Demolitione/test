﻿using UnityEngine;

public class CameraFollowing : MonoBehaviour
{
    public GameObject followingObject;
    private Vector3 pos;

    void Update()
    {
        pos = followingObject.transform.position;
        transform.position = new Vector3(pos.x, pos.y, -10);
    }
}
