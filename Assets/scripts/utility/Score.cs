﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Score : MonoBehaviour
{
    public static Score Instance;

    private void Start()
    {
        Instance = this;
    }

    public int scoreCount;
    public Text score;

    public void SendScore()
    {
        FBmanager.Instance.Analitics(scoreCount);
    }

    public void AddScore()
    {
        scoreCount++;
    }

    void Update()
    {
        
        score.text = "Score: " + scoreCount + " ";
    }
}
