﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletFly : MonoBehaviour
{


    void Start()
    {
        gameObject.GetComponent<Rigidbody2D>().AddForce(transform.right * 500, ForceMode2D.Force);
        StartCoroutine(DestroyBullet());
    }

    IEnumerator DestroyBullet()
    {
        yield return new WaitForSeconds(2);
        Destroy(gameObject);
    }

}
