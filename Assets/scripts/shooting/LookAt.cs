﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookAt : MonoBehaviour
{
    private GameObject target;
    private GameObject fakeTarget;
    private Vector2 pos;
    private Vector3 vertic;
    private Vector3 horiz;
    public float angle;
    public bool changeTarget;

    void Start()
    {
        horiz = gameObject.transform.right;
        vertic = gameObject.transform.up;
        target = GameObject.Find("Holder");
        fakeTarget = GameObject.Find("Fake_target");
    }

    public void ChangeToFake()
    {
        changeTarget = true;
    }

    public void ChangeToMain()
    {
        changeTarget = false;
    }

    void Update()
    {
        if (changeTarget == true)
        {
            pos = fakeTarget.transform.position;
        }
        else
        {
            pos = target.transform.position;
        }

        float lookAngle = (Vector2.Angle(horiz, (new Vector3(pos.x,pos.y,0) - transform.position)));

        if ((Vector2.Angle(vertic, (new Vector3(pos.x, pos.y, 0) - transform.position))) > 90)
        {
            transform.rotation = Quaternion.Euler(0, 0, -lookAngle + angle);
        }
        else
        {
            transform.rotation = Quaternion.Euler(0, 0, lookAngle + angle);
        }

        
        
    }
}
