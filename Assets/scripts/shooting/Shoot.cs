﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Shoot : MonoBehaviour
{
    public bool fireState;
    public Text ammoBar;
    public int ammo;
    public GameObject playerGun;

    public GameObject bulletPrefab;

    private void Start()
    {
        ammoBar.text = " Ammo: " + ammo;
    }

    public void FireEnable()
    {
        fireState = true;
    }

    public void FireDisable()
    {
        fireState = false;
    }

    public void Reloading()
    {
        StartCoroutine(ReloadingAmmo());
    }

    public void PreShoot()
    {
        Instantiate(bulletPrefab, transform.position, transform.rotation);
    }

    public void CreateBullet()
    {
        if ((fireState == true) && (ammo >= 1))
        {
            Instantiate(bulletPrefab, transform.position, transform.rotation);
            ammo--;
            ammoBar.text = " Ammo: " + ammo;
            if (gameObject == playerGun)
            {
                StartCoroutine(localReload());
                fireState = false;
            }
            


        }
        
    }

    IEnumerator localReload()
    {
        yield return new WaitForSeconds(0.5f);
        fireState = true;
    }

    IEnumerator ReloadingAmmo()
    {
        Color oldColor = ammoBar.color;
        ammoBar.color = Color.red;
        ammoBar.text = "Reloading";
        ammo = 0;
        yield return new WaitForSeconds(1);
        ammo = 6;
        ammoBar.color = oldColor;
        ammoBar.text = " Ammo: " + ammo;
    }
}
