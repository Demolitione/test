﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Events;


public class PlayerDead : MonoBehaviour
{
    public GameObject parent;
    public UnityEvent GameEnd;
    public GameObject endScreen;


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if ((collision.gameObject.tag == "projectile") && (collision.gameObject.name.Contains("enemy") == true))
        {
            Debug.Log("Dead");
            
            Instantiate(endScreen, transform.position, Quaternion.Euler(0, 0, 0));
            GetComponent<BoxCollider2D>().isTrigger = false;
            GetComponent<CircleCollider2D>().isTrigger = false;
            parent.GetComponent<Animator>().enabled = false;
            GetComponent<Rigidbody2D>().gravityScale = 1;
            GameEnd.Invoke();

            //StartCoroutine(Restart());
        }
         
    }
   
    

    /*
    IEnumerator Restart()
    {
        yield return new WaitForSeconds(1);
        GameEnd.Invoke();
        Instantiate(endScreen, transform.position, Quaternion.Euler(0, 0, 0));
        
    }*/
}
