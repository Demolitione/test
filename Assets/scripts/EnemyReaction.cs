﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;


public class EnemyReaction : MonoBehaviour
{
    public GameObject particlePrefab;
    public UnityEvent PlayerFail;
    public UnityEvent PlayerSuccess;
    public bool playerKill;
    public GameObject weapon;


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "projectile")
        {
            Vector3 pos = collision.gameObject.transform.position;
            Quaternion rot = collision.gameObject.transform.rotation;
            Instantiate(particlePrefab, new Vector3(pos.x, pos.y, pos.z -5), Quaternion.Euler(rot.x, 90, rot.z));
            playerKill = true;
            weapon.GetComponent<LookAt>().enabled = false;
            weapon.GetComponent<Rigidbody2D>().gravityScale = 1;
            weapon.GetComponent<PolygonCollider2D>().enabled = true;
        }
        else
        {
            if (collision.gameObject.tag == "bomb")
            {
                Debug.Log("boom");
                playerKill = true;
                weapon.GetComponent<LookAt>().enabled = false;
                weapon.GetComponent<Rigidbody2D>().gravityScale = 1;
                weapon.GetComponent<PolygonCollider2D>().enabled = true;
            }
        }
        
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.name == "marker")
        {
            if ((gameObject.GetComponent<BoxCollider2D>().IsTouching(collision) == false) && (gameObject.GetComponent<CircleCollider2D>().IsTouching(collision) == false))
            {
                StartCoroutine(FailEvent(collision.gameObject));
            }
        }
        else
        {
            if (collision.gameObject.tag == "projectile")
            {
                Vector3 pos = collision.gameObject.transform.position;
                Quaternion rot = collision.gameObject.transform.rotation;
                Instantiate(particlePrefab, new Vector3(pos.x, pos.y, pos.z - 5), Quaternion.Euler(rot.x, -90, rot.z));
            }
        }

    }
    IEnumerator FailEvent(GameObject marker)
    {
        yield return new WaitForSeconds(0.5f);
        
        if (playerKill == false)
        {
            PlayerFail.Invoke();
        }
        else
        {
            marker.SetActive(false);
            PlayerSuccess.Invoke();
        }
            
        




    }

}
