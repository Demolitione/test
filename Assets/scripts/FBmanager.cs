﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Facebook.Unity;

public class FBmanager : MonoBehaviour
{
    public static FBmanager Instance;


    // Include Facebook namespace


    // Awake function from Unity's MonoBehavior
    void Awake()
{
        DontDestroyOnLoad(this);
        Instance = this;
        if (!FB.IsInitialized)
    {
        // Initialize the Facebook SDK
        FB.Init(InitCallback, OnHideUnity);
    }
    else
    {
        // Already initialized, signal an app activation App Event
        FB.ActivateApp();
    }

        
}

private void InitCallback()
{
    if (FB.IsInitialized)
    {
        // Signal an app activation App Event
        FB.ActivateApp();
        // Continue with Facebook SDK
        // ...
    }
    else
    {
        Debug.Log("Failed to Initialize the Facebook SDK");
    }
}

private void OnHideUnity(bool isGameShown)
{
    if (!isGameShown)
    {
        // Pause the game - we will need to hide
        Time.timeScale = 0;
    }
    else
    {
        // Resume the game - we're getting focus again
        Time.timeScale = 1;
    }
}
    public void Analitics(int arg)
    {
        var tutParams = new Dictionary<string, object>();
        tutParams["arg data"] = arg.ToString();

        FB.LogAppEvent("Data", parameters: tutParams);
        Debug.Log("Data sended: " + arg);
    }

}
