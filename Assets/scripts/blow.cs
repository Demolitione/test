﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class blow : MonoBehaviour
{
    public GameObject blowPrefab;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "projectile")
        {
            StartCoroutine(BlowUP());
        }
    }

    IEnumerator BlowUP()
    {

        GetComponent<CircleCollider2D>().enabled = true;
        GetComponent<CapsuleCollider2D>().enabled = true;
        GetComponent<SpriteRenderer>().enabled = false;
        
        Instantiate(blowPrefab, transform.position, Quaternion.Euler(-180,0,0));
        yield return new WaitForSeconds(1);
        GetComponent<CircleCollider2D>().enabled = false;
        Destroy(gameObject);
    }
}
