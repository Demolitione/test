﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class EndScreen : MonoBehaviour
{
    private void Start()
    {
        GetComponent<Text>().text = "Score: " + Score.Instance.scoreCount;
    }

    public void DoubleScore()
    {
        GetComponent<Text>().text = "Score: " + (Score.Instance.scoreCount*2);
    }

    public void Restart()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void Menu()
    {
        SceneManager.LoadScene("menu");
    }
}
